const path = require('path');

module.exports = {
    root: true,
    parser: '@typescript-eslint/parser',
    plugins: [
      '@typescript-eslint',
    ],
    extends: [
      'airbnb-base'
    ],
    rules: {
      'no-unused-vars': 'off',
      'import/no-extraneous-dependencies': 'off',
    },
    settings: {
      'import/resolver': {
        alias: {
          map: [
            ['@', path.resolve(__dirname, 'src')],
          ],
          extensions: ['.ts', '.js', '.json']
        }
      }
    }
  };
