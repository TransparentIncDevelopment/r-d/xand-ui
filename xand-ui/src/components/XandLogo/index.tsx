import React from 'react';
import styled, { css } from 'styled-components';

import { ReactComponent as XandLogoBlack } from '../../images/logo/xand_logo_black.svg';
import { ReactComponent as XandTextBlack } from '../../images/logo/xand_text_black.svg';
import { ReactComponent as XandTextLogoBlack } from '../../images/logo/xand_text_logo_black.svg';
import Colors from '../../constants/colors';

export interface XandLogoProps {
  className?: string;
  fillColor?: 'red' | 'black' | 'brown' | 'gold' | 'default',
  variant: 'text' | 'logo' | 'textlogo',
}

const StyledXandLogoContainer = styled.div<XandLogoProps>`
  height: 100%;
  width: 100%;

    & > svg {
      height: 100%;
      width: 100%;
    }
    
    & > * {
    fill: ${(props: XandLogoProps) => {
    switch (props.fillColor) {
      case 'red':
        return Colors.primary.red[100];
      case 'brown':
        return Colors.primary.brown[100];
      case 'gold':
        return Colors.primary.gold[100];
      case 'black':
      case 'default':
      default:
        return Colors.primary.black[100];
    }
  }
}
}
`;

export default function XandLogo(props: XandLogoProps): JSX.Element {
  const { className, fillColor, variant } = props;

  function chooseSvg() {
    switch (variant) {
      case 'text':
        return <XandTextBlack />;
      case 'logo':
        return <XandLogoBlack />;
      case 'textlogo':
        return <XandTextLogoBlack />;
      default:
        return <XandLogoBlack />;
    }
  }

  return (
    <StyledXandLogoContainer
      variant={variant}
      fillColor={fillColor}
      className={className}
    >
      {chooseSvg()}
    </StyledXandLogoContainer>
  );
}
