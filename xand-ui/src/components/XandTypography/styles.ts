import { css } from 'styled-components';

import Sizes from '../../constants/sizes';

const baseCss = css`
  font-family: 'Telegraf';
  margin: 0;
  padding: 0;
`;

const jumboCss = css`
  ${baseCss}
  font-weight: ${Sizes.font.jumbo.weight};
  font-size: ${Sizes.font.jumbo.size};
  line-height: ${Sizes.font.jumbo.height};
`;

const h1Css = css`
  ${baseCss}
  font-weight: ${Sizes.font.title.weight};
  font-size: ${Sizes.font.title.size};
  line-height: ${Sizes.font.title.height};
`;

const h2Css = css`
  ${baseCss}
  font-weight: ${Sizes.font.subtitleBold.weight};
  font-size: ${Sizes.font.subtitleBold.size};
  line-height: ${Sizes.font.subtitleBold.height};
`;

const h3Css = css`
  ${baseCss}
  font-weight: ${Sizes.font.subtitleLight.weight};
  font-size: ${Sizes.font.subtitleLight.size};
  line-height: ${Sizes.font.subtitleLight.height};
`;

const h4Css = css`
  ${baseCss}
  font-weight: ${Sizes.font.largeText.weight};
  font-size: ${Sizes.font.largeText.size};
  line-height: ${Sizes.font.largeText.height};
`;

const h5Css = css`
  ${baseCss}
  font-weight: ${Sizes.font.text.weight};
  font-size: ${Sizes.font.text.size};
  line-height: ${Sizes.font.text.height};
`;

const h6Css = css`
  ${baseCss}
  font-weight: ${Sizes.font.small.weight};
  font-size: ${Sizes.font.small.size};
  line-height: ${Sizes.font.small.height};
`;

const largeBodyCss = h4Css;
const bodyCss = h5Css;
const smallBodyCss = h6Css;

export default {
  jumboCss, h1Css, h2Css, h3Css, h4Css, h5Css, h6Css, largeBodyCss, bodyCss, smallBodyCss,
};
