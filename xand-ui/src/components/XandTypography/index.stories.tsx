import React from 'react';
import { Meta, Story } from '@storybook/react';

import XandTypography, { XandTypographyProps } from '.';

export default {
  component: XandTypography,
  title: 'XandTypography',
} as Meta;

const Template: Story<XandTypographyProps> = (args) => <XandTypography {...args}>This is a test</XandTypography>;
export const Configurable = Template.bind({});
Configurable.args = {
  variant: 'body',
};

export const Headings: Story<XandTypographyProps> = () => (
  <>
    <XandTypography variant="jumbo">H1: Jumbo</XandTypography>
    <XandTypography variant="h1">H1: Title</XandTypography>
    <XandTypography variant="h2">H2: Subtitle 01</XandTypography>
    <XandTypography variant="h3">H3: Subtitle 02</XandTypography>
    <XandTypography variant="h4">H4: Large Text</XandTypography>
    <XandTypography variant="h5">H5: Text</XandTypography>
    <XandTypography variant="h6">H6: Small Text / Annotations</XandTypography>
  </>
);
export const Bodies: Story<XandTypographyProps> = () => (
  <>
    <XandTypography variant="body-large">Large Body Text </XandTypography>
    <XandTypography variant="body">Regular Body Text</XandTypography>
    <XandTypography variant="body-small">Small Body Text / Annotations</XandTypography>
  </>
);

export const LoremIpsum: Story<XandTypographyProps> = () => (
  <>
    <XandTypography variant="h1">Lorem Ipsum</XandTypography>
    <XandTypography variant="h3">Varius Sit Amet Mattis Vulputate</XandTypography>
    <XandTypography variant="body">
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
      labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
      laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in
      voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat
      non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
    </XandTypography>
    <XandTypography variant="body-small">
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
      tempor incididunt ut labore et dolore magna aliqua.
      Sodales ut eu sem integer.
    </XandTypography>
  </>
);
