// https://babeljs.io/docs/en/babel-polyfill
import 'core-js/stable';
import 'regenerator-runtime/runtime';

import Counter from './components/Counter';
import XandLogo from './components/XandLogo';
import XandTypography from './components/XandTypography';

import GlobalXandStyles from './styles/XandGlobalStyles';
import XandBaseStyle from './styles/XandGlobalStyles/XandBaseStyle';
import XandFontFaces from './styles/XandGlobalStyles/XandFontFaces';

import Colors from './constants/colors';
import Sizes from './constants/sizes';
import GridSystem from './constants/gridSystem';
import Breakpoints from './constants/breakpoints';

export const Components = {
  GlobalXandStyles,
  Counter,
  XandLogo,
  XandTypography,
};

export const Constants = {
  Colors, Sizes, GridSystem, Breakpoints,
};

export const Style = { GlobalXandStyles, XandBaseStyle, XandFontFaces };

export {
  GlobalXandStyles,
  Counter,
  XandLogo,
  XandTypography,
  Colors,
  Sizes,
  GridSystem,
  Breakpoints,
  XandBaseStyle,
  XandFontFaces,
};

export default Components;
