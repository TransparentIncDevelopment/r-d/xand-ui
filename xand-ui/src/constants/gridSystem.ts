export default {
  default: {
    cols: 4,
    margins: '1rem',
    gutters: '1rem',
  },
  tablet: {
    cols: 8,
    margins: '1rem',
    gutters: '1rem',
  },
  tabletWide: {
    cols: 8,
    margins: '1.5rem',
    gutters: '1.5rem',
  },
  desktop: {
    cols: 12,
    margins: '1.5rem',
    gutters: '1.5rem',
  },
  desktopWide: {
    cols: 12,
    margins: '1.5rem',
    gutters: '1.5rem',
  },
};
