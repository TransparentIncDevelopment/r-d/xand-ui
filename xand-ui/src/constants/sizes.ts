export default {
  icon: {
    // TODO
    small: '1rem',
    default: '1.5rem',
    large: '2.75rem',
    xlarge: '4.5rem',
  },
  font: {
    jumbo: { size: '6.25rem', weight: 400, height: 1.1 },
    title: { size: '5rem', weight: 400, height: 1.2 },
    subtitleBold: { size: '2.5rem', weight: 600, height: 1.25 },
    subtitleLight: { size: '2.5rem', weight: 300, height: 1.25 },
    largeText: { size: '1.75rem', weight: 400, height: 1.3 },
    text: { size: '1.5rem', weight: 400, height: 1.3 },
    small: { size: '1rem', weight: 400, height: 1.5 },
    default: '1.5rem',
    base: '100%',
  },
};

// Sizes from AI:
// H1 Title: 100pt Telegraf
// H2 SubTitle1: 50pt Telegraf semi-bold
// H3 Subtitle2: 50pt Telegraf light
// H4 Body1: 30pt Telegraf
// H5 Body2: 20pt Telegraf
// H6 Annotation: 18pt Telegraf
// Translate to rem based on 100% @ 16px font base browsers
// Should be 50% progression which is the design intention
