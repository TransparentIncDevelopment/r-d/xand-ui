export default {
  tablet: '30em', // 480px @ 16px base
  tabletWide: '48em', // 768px
  desktop: '62em', // 992px
  desktopWide: '75em', // 1200px
};
