import { createGlobalStyle } from 'styled-components';

const XandBaseStyle = createGlobalStyle`
html {
  font-family: 'Telegraf', 'sans serif';
  font-size: 100%;
  height: 100%;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}
`;

export default XandBaseStyle;
