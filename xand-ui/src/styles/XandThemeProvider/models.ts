interface XandTheme {
  primary: string,
  seconary: string,
  tertiary: string,
  accent: string,
  action: string,
  text: string,
  textInverted: string,
  black: string,
  white: string,
}

export default XandTheme;
