import { addDecorator } from '@storybook/react';

import XandGlobalStyles from '../src/styles/XandGlobalStyles';

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
}

// Add the global styles to storybook
addDecorator(story => <><XandGlobalStyles />{story()}</>);
