# Dependencies

Make sure that you have the peer dependencies of the project installed in the consuming project.

# Installation

Xand-UI includes an es6 module as well as a commonjs package.  See `package.json` for entrypoints.

```bash
yarn add xand-ui
# - OR -
yarn add xand-ui@beta
```

Use a component from the library by importing the module:
```js
import { XandLogo } from 'xand-ui';
```

The constant values that the library uses as its `styled-components` values are also available via `Constants`:

```js
import { Constants } from 'xand-ui';

var example = Constants.Colors.primary.red[100];
```

Then, you can use any of the components freely in your React project:

```jsx
export default function Component(props: ComponentProps): JSX.Element {
  return (
    <div>
      <XandLogo fillColor="red" />
    </div>
  );
}
```

# Actions

`yarn install` will install all dependencies.

#### General

- `yarn clean` - Clean all temp files
- `yarn lint` - Lint the project
- `yarn fix` - Lint and autofix
- `yarn test` - Run jest tests

#### Building

- `yarn build` - Build to `dist`
- `yarn rebuild` - Cleans, reinstalls dependencies, and rebuilds
- `yarn build-storybook` - Builds storybook website

#### Running

- `yarn storybook` - Runs storybook locally

# Configuration

### Enrivonment

`.nvmrc` - Tells `nvm` which version of node to `use` (if you use `nvm`)

`.yarnclean` - Removes react-native type declaration files which are automatically installed with `@types/styled-components`

### Testing

`jest.config.js` - Jest testing configuration

### Linting

`.eslintrc.js` + `.eslintignore` - ESLint configuration for Xand-UI

### Bundling + Transpiling

`rollup.config.js` - Rollup is used for bundling the library and uses plugins such as `@rollup/plugin-babel` to transpile as well through `babel`

`tsconfig.json` - Typescript configuration, used for type declaration file generation during building

`.babelrc` - Babel configuration for transpiling Source ES6 code into JS that is usable by web browsers.  Plugin order is important

`.browserslistrc` - Configures which browsers to target during transpiling with babel

## Consuming Xand-UI Package Locally During Development

Some npm packages such as styled-components and react that are used as singletons can break local builds if an npm package is linked locally in package.json via `file:foo` or `link:foo`.  To avoid multiple instances being loaded from each project's respective node_modules folder, make sure that the consuming project aliases the peer dependencies of `xand-ui` to always point to its own node_modules.  

Here is an example of consuming the library locally for dev testing when the consuming project uses webpack:

```json
# package.json
...
    dependencies: {
        ...
        "xand-ui": "link:/path/to/local/xand-ui",
    }
...
```

```js
# webpack

module.exports = {
    ...

    resolve: {
        alias: {
            // For using local linked library build during development
            // https://styled-components.com/docs/faqs#how-can-i-fix-issues-when-using-npm-link-or-yarn-link
            'react-dom': path.resolve(__dirname,'./node_modules/react-dom'),
            'react': path.resolve(__dirname,'./node_modules/react'),
            'styled-components': path.resolve(__dirname,'./node_modules/styled-components'),
        },
    }
    ...
}

```
